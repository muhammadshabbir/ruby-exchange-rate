# http://test-unit.github.io/
# http://test-unit.github.io/
require 'test/unit'
require_relative '../lib/currency_exchange'

class CurrencyExchangeTest < Test::Unit::TestCase
  def setup
    @data = CurrencyExchange::DataJson.new(file:  "data/eurofxref-hist-90d.json")
  end

  def test_exchange_rate
    # Test edge cases
    assert_raises(ArgumentError) { @data.rate('2018-09-12') }
    assert_raises(ArgumentError) { @data.rate('USD', 'GBP') }

    # Test invalid date
    assert_raises(RuntimeError) { @data.rate('2021-12-25', 'USD', 'GBP') }

    # Test invalid currency
    assert_raises(RuntimeError) { @data.rate('2018-09-13', 'XXX', 'GBP') }
    assert_raises(RuntimeError) { @data.rate('2018-09-13', 'USD', 'YYY') }

    # Test valid conversions
    assert_in_delta(1.162, @data.rate('2018-09-13', 'EUR', 'USD'), 0.001)
    assert_in_delta(1.148725, @data.rate('2018-11-09', 'GBP', 'EUR'), 0.001)
    assert_in_delta(0.00887103036, @data.rate('2018-12-10', 'JPY', 'USD'), 0.001)
  end
end

