require_relative "currency_exchange"
include CurrencyExchange

data = DataJson.new(:file => "data/eurofxref-hist-90d.json", :currency => "EUR")

date = ""
to_currency = ""
from_currency = ""

loop do
  data.base
  puts "Enter \"quit\" any time to exit"

  print "Enter a date in this format (yyyy-mm-dd): "
  date = gets.chomp.strip.upcase
  break if date == "QUIT"

  print "Enter the currency you wish to convert from (enter the currency code): "
  from_currency = gets.chomp.strip.upcase
  break if from_currency == "QUIT"

  print "Enter the currency you wish to convert to (enter the currency code): "
  to_currency = gets.chomp.strip.upcase
  break if to_currency == "QUIT"

  data.rate(date, from_currency, to_currency)
end
