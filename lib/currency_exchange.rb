module CurrencyExchange
  require 'json'
  # Return the exchange rate between from_currency and to_currency on date as a float.
  # Raises an exception if unable to calculate requested rate.
  # Raises an exception if there is no rate for the date provided.
  def self.rate(date = nil, from_currency = nil, to_currency = nil)
    # TODO: calculate and return rate
    "SYSTEM ERROR: method missing"
  end

  #Data.extend(CurrencyExchange)

  class Data
    def initialize(args)
      @file = args.fetch(:file, "data/eurofxref-hist-90d.json")
      @currency = args.fetch(:currency, "EUR").strip
    end

    def base
      puts "The currency exchange is based on the #{@currency}"
    end

    def rate()
      CurrencyExchange.rate()
    end
  end

  class DataJson < Data
    def read_file
      begin
        file = File.open @file
        data = JSON.load file
      rescue Errno::ENOENT => e
        puts "File not found: #{e.message}"
      ensure
        file&.close
        abort unless file
        data
      end
    end

    def rate(date, from_currency, to_currency)
      data = read_file
      raise "#{date} is not in the data source" if data[date].nil?



      raise "#{to_currency} and/or #{from_currency} are not in the data source" if
        data[date][to_currency].nil? and to_currency != "#{@currency}"

      raise "#{to_currency} and/or #{from_currency} are not in the data source" if 
        data[date][from_currency].nil? and from_currency != "#{@currency}"

      if from_currency == "#{@currency}"
        from = 1
      else
        from = data[date][from_currency]
      end

      if to_currency == "#{@currency}"
        to = 1
      else
        to = data[date][to_currency]
      end

      exchange_rate = to / from
      puts "1 #{from_currency} = #{exchange_rate} #{to_currency}"
      puts
      exchange_rate
    end
  end
end
