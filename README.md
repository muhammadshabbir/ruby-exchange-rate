Run the code from the top of the directory tree as it uses relative file paths.
This code calculates the exchange rate between two currencies and is based on the EUR.
It is also designed to raise exceptions when a missing date, file or currency is entered.